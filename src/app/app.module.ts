import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { environment } from 'environments/environment';
import { ROUTES } from './app.routes';

import '../styles/styles.scss';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SelectModule } from './select/select.module';
import { ReactiveFormsModule } from '@angular/forms';

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules,
    }),
    
    SelectModule,
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    environment.ENV_PROVIDERS,
  ],
})
export class AppModule {
}
