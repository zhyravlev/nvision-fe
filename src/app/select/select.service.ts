import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SelectService {
  
  private api = 'http://localhost:8080/';
  
  constructor(private http: HttpClient) {
  }
  
  public getData(search: string, exclude: number[]): Observable<Select.Model[]> {
    
    const headers = new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json');
    
    const params = new HttpParams()
      .set('search', search)
      .set('exclude', JSON.stringify(exclude));
    
    return this.http.get<Select.Model[]>(this.api, {params, headers});
  }
}
