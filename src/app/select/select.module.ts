import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select.component';
import { SelectService } from './select.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  declarations: [
    SelectComponent,
  ],
  exports: [
    SelectComponent,
  ],
  providers: [
    SelectService,
  ],
})
export class SelectModule {
}
