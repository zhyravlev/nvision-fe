declare enum UI_TYPES {
  MULTI = 'multi',
  SINGLE = 'single'
}

declare enum ACCESS_LEVEL {
  owner = 1,
  admin = 2,
  manager = 3
}
