declare namespace Select {
  export interface Model {
    id?: number;
    name?: string;
    registration?: string;
    factory?: string;
    fiscalStorage?: string;
    accessLevel?: ACCESS_LEVEL;
  }

  export interface Options {
    type: UI_TYPES;
  }
}
