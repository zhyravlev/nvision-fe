import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  HostListener,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { SelectService } from './select.service';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as _ from 'lodash';
import 'rxjs/add/operator/debounceTime';
import { Subject } from 'rxjs/Subject';
import { UI_TYPES } from './types';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: [
    './select.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
  ],
})
export class SelectComponent implements ControlValueAccessor, OnInit {
  
  public isOpen: boolean = false;
  
  public firstLoad: boolean = false;
  
  public loadingInProgress: boolean = false;
  
  public searchResult: Select.Model[] = [];
  
  public searchInput: FormControl = new FormControl('');
  
  /**
   * Опции компонента
   */
  @Input()
  public options: Select.Options;
  
  /**
   * Ссылка на строку поиска
   */
  @ViewChild('inputElement')
  public inputElement: ElementRef;
  
  constructor(private api: SelectService,
              private elementRef: ElementRef,
              private cdr: ChangeDetectorRef) {
  }
  
  public ngOnInit(): void {
    
    /**
     * Отслеживаем изменения поля поиска и получаем данные с API
     */
    this.searchInput
      .valueChanges
      .debounceTime(300) // небольшая задержка в 300мс для исключения шквала запросов
      .subscribe(() => {
        this.getData();
        this.cdr.markForCheck();
      });
    
    this.menuStateEvent
      .subscribe(() => {
        /**
         * Получим список первых 5 элементов только когда открыли меню,
         * исключая ненужную загрузку при инициализации компонента
         */
        if (!this.firstLoad && this.isOpen) {
          this.getData();
          this.firstLoad = true;
        }
        
        /**
         * Ставим фокус на поле поиска при открытии меню
         */
        if (this.isOpen) {
          setTimeout(() => this.inputElement.nativeElement.focus(), 0);
        }
        
        /**
         * Сбросить поле поиска при закрытии и актуализовать данные
         */
        if (!this.isOpen) {
          this.searchInput.setValue('', {onlySelf: false, emitEvent: false});
          this.getData();
        }
      });
    
    if (!_.has(this.options, 'type') || !_.includes(['multi', 'single'], this.options.type)) {
      throw Error('Undefined type of select component!');
    }
  }
  
  public writeValue(value: any): void {
    this.value = value;
  }
  
  public registerOnChange(fn): void {
    this.propagateChange = fn;
  }
  
  public registerOnTouched(): void {
  }
  
  public set value(value: any) {
    this.currentValue.setValue(value);
    this.propagateChange(value);
  }
  
  public get value(): any {
    return this.currentValue.value;
  }
  
  /**
   * Выбор всех элементов
   */
  public selectAll(): void {
    this.value = null;
    this.isOpen = false;
  }
  
  /**
   * Подготовка лейбла
   * @returns {string}
   */
  public get label(): string {
    if (_.isArray(this.value)) {
      let names = [];
      
      _.forEach(this.value, (item: Select.Model) => {
        names.push(item.name);
      });
      
      return _.join(names, ', ');
    }
    
    return 'Все';
  }
  
  /**
   * Открыть / закрыть меню при клике по кнопке
   */
  public openMenu(): void {
    this.isOpen = !this.isOpen;
    this.menuStateEvent.next();
  }
  
  /**
   * Выбор элемента меню
   * @param {Select.Model} item
   */
  public selectItem(item: Select.Model): void {
    
    // мульти селект
    if (this.options.type === UI_TYPES.MULTI) {
      
      if (_.isNull(this.value)) {
        this.value = [item];
      }
      
      else if (_.isArray(this.value)) {
        let value = _.cloneDeep(this.value);
        value.push(item);
        
        this.value = value;
      }
      
    }
    
    // сингл селект
    else if (this.options.type === UI_TYPES.SINGLE) {
      this.value = [item];
    }
    
    this.getData();
  }
  
  /**
   * Удаление элемента меню
   * @param {Select.Model} item
   */
  public removeItem(item: Select.Model): void {
    let value = _.cloneDeep(this.value);
    
    _.remove(value, (o: Select.Model) => o.id === item.id);
    
    if (_.size(value) === 0) {
      this.value = null;
    } else {
      this.value = value;
    }
    
    this.getData();
  }
  
  /**
   * Если есть выбранные элементы
   * @returns {boolean}
   */
  public get hasSelectedItems(): boolean {
    return _.isArray(this.value);
  }
  
  /**
   * Закрыть меню при клике вне него
   * @param event
   */
  @HostListener('document:click', ['$event'])
  public onWindowClick(event: MouseEvent): void {
    const wrapper = this.elementRef.nativeElement;
    
    if (this.isOpen && !wrapper.contains(event.target) && (<any>event.target).className.indexOf('select-menu__item-handler') === -1) {
      this.isOpen = false;
      this.menuStateEvent.next();
    }
  }
  
  // ***** Приватные свойства
  
  /**
   * Модель данных
   * @type {FormControl}
   */
  private currentValue = new FormControl();
  
  private menuStateEvent = new Subject();
  
  private propagateChange = (_: any) => {
  };
  
  // ***** Приватные методы
  
  /**
   * Получаем актуальные данные с API
   */
  private getData(): void {
    this.loadingInProgress = true;
    
    this.api.getData(this.searchInput.value, this.selectedId)
      .subscribe((data: any) => {
        this.searchResult = data;
        this.loadingInProgress = false;
        this.cdr.markForCheck();
      });
  }
  
  /**
   * Получаем массив id выбранных элементов
   * @returns {number[]}
   */
  private get selectedId(): number[] {
    const result = [];
    
    _.forEach(this.value, (item: Select.Model) => {
      result.push(item.id);
    });
    
    return result;
  }
}
