import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  
  public select: FormControl;
  
  constructor() {
  }
  
  public ngOnInit(): void {
    this.select = new FormControl(
      [{
        'id': 1,
        'name': 'Дмитрий Антонов',
        'registration': '1EU15RTJ1OSH',
        'factory': '4BSBUN07TEK',
        'fiscalStorage': 'PJ2FJWB7BFO',
        'accessLevel': 'owner',
      }],
    );
  
    this.select.valueChanges.subscribe((value: any) => console.log('::: valueChanges', value));
  }
}
